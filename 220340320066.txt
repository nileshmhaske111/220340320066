SECTION 1

Q 1. Create table DEPT with the following structure:-
 
 CREATE DATABASE exam;
 CREATE TABLE dept(deptno INT(2), dname VARCHAR(15), loc VARCHAR(10));
 
 INSERT INTO dept VALUES(10, 'ACCOUNTING', 'NEW YORK'), (20, 'RESEARCH', 'DALLAS'), (30, 'SALES', 'CHICAGO'), (40, 'OPERATIONS', 'BOSTON');
 
 ANS:
 +--------+------------+----------+
| deptno | dname      | loc      |
+--------+------------+----------+
|     10 | ACCOUNTING | NEW YORK |
|     20 | RESEARCH   | DALLAS   |
|     30 | SALES      | CHICAGO  |
|     40 | OPERATIONS | BOSTON   |
+--------+------------+----------+
 
 -----------------------------------------------------------------------------
 Q 2. Create table EMP with the following structure:-
 
 CREATE TABLE emp(empno INT(4), ename VARCHAR(10), job VARCHAR(9), hiredate DATE, sal FLOAT(7,2), comm FLOAT(7,2), deptno INT(2));
 
INSERT INTO emp VALUES
(7839, 'KING', 'MANAGER', "1991-11-17", 5000, NULL, 10),
(7698, 'BLAKE', 'CLERK', "1981-05-01", 2850, NULL, 30),
(7782, 'CLARK', 'MANAGER', "1981-06-09", 2450, NULL, 10),
(7566, 'JONES', 'CLERK', "1981-04-02", 2975, NULL, 20),
(7654, 'MARTIN', 'SALESMAN', "1981-09-28", 1250, 1400, 30),
(7499, 'ALLEN', 'SALESMAN', "1981-02-20", 1600, 300, 30);

ANS:

+-------+--------+----------+------------+---------+---------+--------+
| empno | ename  | job      | hiredate   | sal     | comm    | deptno |
+-------+--------+----------+------------+---------+---------+--------+
|  7839 | KING   | MANAGER  | 1991-11-17 | 5000.00 |    NULL |     10 |
|  7698 | BLAKE  | CLERK    | 1981-05-01 | 2850.00 |    NULL |     30 |
|  7782 | CLARK  | MANAGER  | 1981-06-09 | 2450.00 |    NULL |     10 |
|  7566 | JONES  | CLERK    | 1981-04-02 | 2975.00 |    NULL |     20 |
|  7654 | MARTIN | SALESMAN | 1981-09-28 | 1250.00 | 1400.00 |     30 |
|  7499 | ALLEN  | SALESMAN | 1981-02-20 | 1600.00 |  300.00 |     30 |
+-------+--------+----------+------------+---------+---------+--------+

Q 3. Display all the employees where SAL between 2500 and 5000 (inclusive of both).

SELECT ename, (sal+IFNULL(comm,0.0)) FROM emp WHERE (sal + IFNULL(comm,0.0)) BETWEEN 2500 AND 5000;

ANS: 
+--------+------------------------+
| ename  | (sal+IFNULL(comm,0.0)) |
+--------+------------------------+
| KING   |                5000.00 |
| BLAKE  |                2850.00 |
| JONES  |                2975.00 |
| MARTIN |                2650.00 |
+--------+------------------------+

4. Display all the ENAMEs in descending order of ENAME.

SELECT ename from emp ORDER BY ename DESC;

ANS:
+--------+
| ename  |
+--------+
| MARTIN |
| KING   |
| JONES  |
| CLARK  |
| BLAKE  |
| ALLEN  |
+--------+

QUE 5. Display all the JOBs in lowercase.

SELECT LOWER(job) FROM emp;

ANS:
+------------+
| LOWER(job) |
+------------+
| manager    |
| clerk      |
| manager    |
| clerk      |
| salesman   |
| salesman   |
+------------+

QUE 6. Display the ENAMEs and the lengths of the ENAMEs.

SELECT ename, LENGTH(ename) 'Length of ename' from emp;

ANS:
+--------+-----------------+
| ename  | Length of ename |
+--------+-----------------+
| KING   |               4 |
| BLAKE  |               5 |
| CLARK  |               5 |
| JONES  |               5 |
| MARTIN |               6 |
| ALLEN  |               5 |
+--------+-----------------+

7. Display the DEPTNO and the count of employees who belong to that DEPTNO .
SELECT deptno, COUNT(*) FROM emp GROUP BY deptno;

ANS:
+--------+----------+
| deptno | COUNT(*) |
+--------+----------+
|     10 |        2 |
|     30 |        3 |
|     20 |        1 |
+--------+----------+


QUE 8. Display the DNAMEs and the ENAMEs who belong to that DNAME.
SELECT dname, ename from emp e CROSS JOIN dept d ON d.deptno = e.deptno;

ANS:
+------------+--------+
| dname      | ename  |
+------------+--------+
| ACCOUNTING | KING   |
| SALES      | BLAKE  |
| ACCOUNTING | CLARK  |
| RESEARCH   | JONES  |
| SALES      | MARTIN |
| SALES      | ALLEN  |
+------------+--------+

QUE 9. Display the position at which the string ‘AR’ occurs in the ename.

SELECT ename, POSITION('AR'IN ename) position FROM emp;

ANS:
+--------+----------+
| ename  | position |
+--------+----------+
| KING   |        0 |
| BLAKE  |        0 |
| CLARK  |        3 |
| JONES  |        0 |
| MARTIN |        2 |
| ALLEN  |        0 |
+--------+----------+

QUE 10. Display the HRA for each employee given that HRA is 20% of SAL.

SELECT ename, sal, (sal * .20) HRA FROM emp;


SECTION 2:

QUE 1 
1. Write a stored procedure by the name of PROC1 that accepts two varchar strings as parameters. Your procedure should then determine if the first varchar string exists inside the varchar string. For example, if string1 = ‘DAC’ and string2 = ‘CDAC, then string1 exists inside string2. The stored procedure should insert the appropriate message into a suitable TEMPP output table. Calling program for the stored procedure need not be written.

CREATE PROCEDURE proc1(st1 VARCHAR(15), st2 VARCHAR(15))
BEGIN
IF (INSTR(st1,st2)>0 OR (instr(st1,st2)>0)) THEN
	INSERT INTO tempp VALUES(st1,st2,"String Exists");
ELSE
	INSERT INTO tempp VALUES(st1,st2,"Not Exists");
END IF;
END; //
delimiter ;

call proc1('DAC','CDAC')
SELECT * FROM tempp;

QUE 2. Create a stored function by the name of FUNC1 to take three parameters, the sides of a triangle. The function should return a Boolean value:- TRUE if the triangle is valid, FALSE otherwise. A triangle is valid if the length of each side is less than the sum of the lengths of the other two sides. Check if the dimensions entered can form a valid triangle. Calling program for the stored function need not be written.

ANS:
DELIMITER //
CREATE FUNCTION func1(a INT, b INT, c INT) RETURNS BOOLEAN DETERMINISTIC 
BEGIN 
	IF NOT (a+b >= c) AND (b+c >= a) AND (c+a >= b)
	THEN RETURN FALSE;
	ELSE RETURN TRUE;
	END IF;
END; //
DELIMITER ;